#!/bin/bash

set -ex

source ./config.sh

version=`cat VERSION`

./build.sh

docker tag $REGISTRY/$USERNAME/$IMAGE:latest $REGISTRY/$USERNAME/$IMAGE:$version

docker push $REGISTRY/$USERNAME/$IMAGE:latest
docker push $REGISTRY/$USERNAME/$IMAGE:$version
