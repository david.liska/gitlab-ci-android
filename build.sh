set -ex

source ./config.sh

docker build -t $REGISTRY/$USERNAME/$IMAGE:latest .
