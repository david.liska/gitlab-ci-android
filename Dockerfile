FROM openjdk:8-jdk

LABEL maintainer="email@davidliska.cz"

ADD VERSION .

ARG ANDROID_COMPILE_SDK="28"
ARG ANDROID_BUILD_TOOLS="28.0.0"
ARG ANDROID_SDK_TOOLS_REV="4333796"

ENV ANDROID_HOME /opt/android-sdk-linux
ENV ANDROID_SDK_HOME $ANDROID_HOME

RUN wget -q https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS_REV}.zip -O android-sdk.zip \
    && unzip -qq android-sdk.zip -d $ANDROID_HOME \
    && rm -f android-sdk.zip

ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools

RUN mkdir $ANDROID_SDK_HOME/.android && \
    echo 'count=0' > $ANDROID_SDK_HOME/.android/repositories.cfg

RUN yes | sdkmanager --licenses
RUN yes | sdkmanager --update

RUN sdkmanager "emulator" "tools" "platform-tools"

RUN yes | sdkmanager \
    "build-tools;$ANDROID_BUILD_TOOLS" \
    "platforms;android-$ANDROID_COMPILE_SDK" \
    "extras;android;m2repository" \
    "extras;google;google_play_services" \
    "extras;google;m2repository"

RUN yes | sdkmanager \
    "system-images;android-22;google_apis;armeabi-v7a"
#    "system-images;android-22;google_apis;x86"

#RUN rm "$ANROID_SDK_HOME/emulator/lib64/libstdc++/libstdc++.so.6" \
#    && ln -s "/usr/lib64/libstdc++.so.6" "$ANDROID_SDK_HOME/emulator/lib64/libstdc++"


#RUN echo no | avdmanager \
#    create avd --name test28 \
#               --package "system-images;android-22;google_apis;x86" \
#               --abi "google_apis/x86"
